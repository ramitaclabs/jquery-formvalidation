
//    To hide error showing paragraph
    $("p").hide();

//  Function to be performed on page load
$(document).ready(function() {
//   When textbox gains focus
    $("input").focus(function() {
        $(this).css("background-color", "lightgrey");
    });

//   When textbox looses focus
    $("input").blur(function() {
        $(this).css("background-color", "#ffffff");
    });

//   To validate the date format in date of birth field
    var date = new Date();
    var days = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    month = month + 1;
    if (month < 10)
    {
        month = '0' + month;
    }
    if (days < 10)
    {
        days = '0' + days;
    }
    var currentdate = (year + "-" + month + "-" + days);
    $("#date").attr('max', currentdate);
    $("#date").attr('min', '1990-01-01');
    
    var test = true;

    $("#pno").keydown(function(e)
    {
//    Code to format phone no. to contain "-"
        if (e.keyCode !== 8) {
            if ($(this).val().length === 0) {
                $(this).val($(this).val() + "(");
            }
            else if ($(this).val().length === 4) {
                $(this).val($(this).val() + ")-");
            } else if ($(this).val().length === 9) {
                $(this).val($(this).val() + "-");
            }
        }
    });

//    Functions to perform on click of submit button
    $("#submit-btn").click(function() {

//    First name column should not be empty and only contain alphabets
        if ($("#f-name").val() === "" || null)
        {
            test = false;
            $("p").show(); //to display error showing paragraph
            document.getElementById("display-error").innerHTML = "Please fill first name";
            return false;

        }
        else {
            var reg = /^[a-zA-Z]*$/;
            test = true;
            if (!reg.test($("#f-name").val())) {
                test = false;
                $("p").show();
                document.getElementById("display-error").innerHTML = "First name can't have numerics or special characters";
                return false;
            }
        }

//     Last name column should not be empty and only contain alphabets
        if ($("#l-name").val() === "" || null)
        {
            test = false;
            $("p").show();
            document.getElementById("display-error").innerHTML = "Please fill last name";
            return false;
        }
        else {
            var reg = /^[a-zA-Z]*$/;

            test = true;
            if (!reg.test($("#l-name").val())) {
                test = false;
                $("p").show();
                document.getElementById("display-error").innerHTML = "Last name can't have numerics or special characters";
                return false;
            }
        }

//      Atleast one gender radio button must be selected
        if (!$('input[name="gender"]:checked').val())
        {
            test = false;
            $("p").show();
            document.getElementById("display-error").innerHTML = "Please select the gender field";
            return false;
        }
        else
        {
            test = true;
        }

//      Date of birth field should not be empty
        if ($("#date").val() === "" || null)
        {
            test = false;
            $("p").show();
            document.getElementById("display-error").innerHTML = "Please fill your date of birth";
            return false;
        }
        else
        {
            test = true;
        }

//      Phone number must be in US format
        if ($("#pno").val() === "" || null)
        {
            test = false;
            $("p").show();
            document.getElementById("display-error").innerHTML = "Please fill phone number";
            return false;
        }
        else
        {
            test = true;
            var tel = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;
            if (!tel.test($("#pno").val()))
            {
                test = false;
                $("p").show();
                document.getElementById("display-error").innerHTML = "Phone number format invalid";
                return false;
            }
        }

//      Email format should be standard
        if ($("#email").val() === "" || null)
        {
            test = false;
            $("p").show();
            document.getElementById("display-error").innerHTML = "Email field can't be empty";
            return false;
        }
        else
        {
            test = true;
            var reg = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!reg.test($("#email").val()))
            {
                test = false;
                $("p").show();
                document.getElementById("display-error").innerHTML = "Incorrect email-id";
                return false;
            }
        }

//      Password should be of greater than 5 characters
        if ($("#pass").val() === "" || null)
        {
            test = false;
            $("p").show();
            document.getElementById("display-error").innerHTML = "Password field can't be empty";
            return false;
        }
        else if ($("#pass").val().length < 5)
        {
            test = false;
            $("p").show();
            document.getElementById("display-error").innerHTML = "Password shoud have more than 5 characters";
            return false;
        }
        else
        {
            test = true;
        }

//      Confirm password should be of greater than 5 characters
        if ($("#confpass").val() === "" || null)
        {
            test = false;
            $("p").show();
            document.getElementById("display-error").innerHTML = "Confirm password field can't be empty";
            return false;
        }
        else
        {
            test = true;
        }

//      Password and confirm password should be same
        if ($("#pass").val() !== $("#confpass").val())
        {
            test = false;
            $("p").show();
            document.getElementById("display-error").innerHTML = "Password and Confirm password don't match";
            return false;
        }
        else
        {
            test = true;
        }

//      Check atleast one hobby
        if ($('input[type=checkbox]:checked').length === 0)
        {
            test = false;
            $("p").show();
            document.getElementById("display-error").innerHTML = "Please select at-least one hobby";
            return false;
        }
        else
        {
            test = true;
        }

        if (test === true)
        {
            alert("Details successfully submitted");
        }
    });
});